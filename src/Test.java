import org.opencv.core.*;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import static org.opencv.core.Core.BORDER_DEFAULT;
import static org.opencv.core.CvType.*;

/**
 * copyright (C), 2018-2019
 * fileName Test
 * author   zhangx
 * date     2019/3/1 14:44
 * https://docs.opencv.org/
 * description
 */
public class Test {

    public String imagePrePath = "D:/tools/ideawork/JavaOpencvTest/dev/";
    //    public String opencvDllPath="D:/tools/opencv/build/java/x64/opencv_java400.dll";
    public String opencvDllPath = "lib/x64/opencv_java400.dll";

    //    D:\tools\opencv\build\java\x64
//    -Djava.library.path=D:\tools\opencv\build\java\x64
    public static void main(String[] args) {
//D:\tools\ideawork\oem-server\JavaOpencvTest
        new Test().testLogEnhance();

    }

    /**
     * 图像增强算法 基于对数Log变换的图像增强 对数变换可以将图像的低灰度值部分扩展，显示出低灰度部分更多的细节，
     * 将其高灰度值部分压缩，减少高灰度值部分的细节，从而达到强调图像低灰度部分的目的
     */
    @Deprecated
    public void testLogEnhance() {//待测试,无法转换，从c++没找到对应的java代码
        setLibPath();
        String imageStr = imagePrePath + "8.jpg";
        Mat src = Imgcodecs.imread(imageStr);

        src.convertTo(src, CV_32F);

        Mat dst = new Mat(src.size(), CV_32F);

            Core.log(src, dst);//这个地方报错
            Core.normalize(dst, dst, 0, 255);
            Core.convertScaleAbs(dst, dst);
            HighGui.imshow("testLogEnhance原图", src);

            dst.convertTo(src, CV_8UC3);
            HighGui.imshow("testLogEnhance", dst);
            HighGui.waitKey(0);
        }

        public void setLibPath () {
            File file = new File(opencvDllPath);
            System.load(file.getAbsolutePath());
        }

        /**
         * 图像增强算法 使用中心为5的8邻域拉普拉斯算子与图像卷积可以达到锐化增强图像的目的
         */
        public void testTePuLaSiEnhance () {
            setLibPath();
//       setLibPath();
            String imageStr = imagePrePath + "9.jpg";
            Mat src = Imgcodecs.imread(imageStr);
            Mat dst = new Mat();


            HighGui.imshow("拉普拉斯算子原图", src);

            Mat kernel = new MatOfFloat(0, -1, 0, 0, 5, 0, 0, -1, 0);
            Imgproc.filter2D(src, dst, CvType.CV_8UC3, kernel);
            HighGui.imshow("拉普拉斯算子2", dst);
            HighGui.waitKey(0);

//        Mat dst=new Mat();
//        if(img.rows()>800||img.cols()>600){
//            Imgproc.resize(img, dst, new Size(600, 800), 0, 0, Imgproc.INTER_AREA);
//            img=dst;
//        }

        }

        /**
         * sobel算法，边缘检测
         * https://www.cnblogs.com/skyfsm/p/6879265.html
         * */
        public void testSobel () {
            setLibPath();
            String imageStr = imagePrePath + "7.jpg";

            Mat src = Imgcodecs.imread(imageStr);
            if (src.empty()) {
                System.err.println("Cannot read image: ");
                System.exit(0);
            }
            Mat grad_x = new Mat();
            Mat grad_y = new Mat();
            Mat abs_grad_x = new Mat();
            Mat abs_grad_y = new Mat();
            Mat dst = new Mat();

            //x方向梯度
            Imgproc.Sobel(src, grad_x, CV_16S, 1, 0, 3, 1, 1, BORDER_DEFAULT);
            Core.convertScaleAbs(grad_x, abs_grad_x);
            HighGui.imshow("x方向", abs_grad_x);

            Imgproc.Sobel(src, grad_y, CV_16S, 0, 1, 3, 1, 1, BORDER_DEFAULT);
            Core.convertScaleAbs(grad_y, abs_grad_y);
            HighGui.imshow("y方向", abs_grad_y);

            Core.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, dst);

            HighGui.imshow("整体方向", dst);

            HighGui.imshow("src", src);

            HighGui.waitKey(0);

        }

        /**
         * 图像旋转
         * https://docs.opencv.org/4.0.0/d4/d61/tutorial_warp_affine.html
         * */
        public void testImageRotationMy () {
            setLibPath();
            String imageStr = imagePrePath + "1.jpg";

            String filename = imageStr;
            Mat src = Imgcodecs.imread(filename);
            if (src.empty()) {
                System.err.println("Cannot read image: " + filename);
                System.exit(0);
            }

            //向左旋转90度,没有黑边
            Point[] srcTri = new Point[3];
            srcTri[0] = new Point(0, 0);
            srcTri[1] = new Point(src.cols() - 1, 0);
            srcTri[2] = new Point(0, src.rows() - 1);

            Point[] dstTri = new Point[3];
            dstTri[0] = new Point(0, src.cols() - 1);
            dstTri[1] = new Point(0, 0);
            dstTri[2] = new Point(src.rows() - 1, src.cols() - 1);

            Mat warpMat = Imgproc.getAffineTransform(new MatOfPoint2f(srcTri), new MatOfPoint2f(dstTri));//转换使用
            Mat warpDst = Mat.zeros(src.cols(), src.rows(), src.type());//转换成的结果
            Imgproc.warpAffine(src, warpDst, warpMat, warpDst.size());


            //向右旋转90度,有黑边
            Point center = new Point(src.cols() / 2, src.rows() / 2);
            double angle = -90.0;
            double scale = 1;
            Mat rotMat = Imgproc.getRotationMatrix2D(center, angle, scale);//翻转矩阵
            Mat rotateMat = new Mat(src.cols(), src.rows(), src.type());
            Imgproc.warpAffine(src, rotateMat, rotMat, src.size());

            HighGui.imshow("Source image", src);
            HighGui.imshow("Warp", warpDst);
            HighGui.imshow("Warp + Rotate", rotateMat);
            HighGui.waitKey(0);
            System.exit(0);

        }


        /**
         * 图像旋转
         * */
        public void testImageRotation () {
            setLibPath();
            String imageStr = imagePrePath + "1.jpg";

            String filename = imageStr;
            Mat src = Imgcodecs.imread(filename);
            if (src.empty()) {
                System.err.println("Cannot read image: " + filename);
                System.exit(0);
            }
            Point[] srcTri = new Point[3];
            srcTri[0] = new Point(0, 0);
            srcTri[1] = new Point(src.cols() - 1, 0);
            srcTri[2] = new Point(0, src.rows() - 1);

            Point[] dstTri = new Point[3];
            dstTri[0] = new Point(0, src.rows() * 0.33);
            dstTri[1] = new Point(src.cols() * 0.85, src.rows() * 0.25);
            dstTri[2] = new Point(src.cols() * 0.15, src.rows() * 0.7);

            Mat warpMat = Imgproc.getAffineTransform(new MatOfPoint2f(srcTri), new MatOfPoint2f(dstTri));
            Mat warpDst = Mat.zeros(src.rows(), src.cols(), src.type());
            Imgproc.warpAffine(src, warpDst, warpMat, warpDst.size());
//        Point center = new Point(warpDst.cols() / 2, warpDst.rows() / 2);

            Point center = new Point(src.cols() / 2, src.rows() / 2);
            double angle = -50.0;
            double scale = 0.6;
            Mat rotMat = Imgproc.getRotationMatrix2D(center, angle, scale);
            Mat warpRotateDst = new Mat();
            Imgproc.warpAffine(warpDst, warpRotateDst, rotMat, warpDst.size());
            HighGui.imshow("Source image", src);
            HighGui.imshow("Warp", warpDst);
            HighGui.imshow("Warp + Rotate", warpRotateDst);
            HighGui.waitKey(0);
            System.exit(0);

        }


        /***
         * 图像翻转 https://blog.csdn.net/jningwei/article/details/78753607
         * flipCode  1	水平翻转  0	垂直翻转 -1	水平垂直翻转
         */
        public void testImageFlip () {
            setLibPath();
            String imageStr = imagePrePath + "1.jpg";
            Mat src = Imgcodecs.imread(imageStr);
            Mat dst = new Mat();

            HighGui.imshow("原图像", src);

            Core.flip(src, dst, 1);

            HighGui.imshow("水平翻转 ", dst);

            dst = new Mat();
            Core.flip(src, dst, 0);
            HighGui.imshow("垂直翻转", dst);

            dst = new Mat();
            Core.flip(src, dst, -1);
            HighGui.imshow("水平垂直翻转", dst);


            HighGui.waitKey();

        }

        /**
         * 直方图均衡化是通过调整图像的灰阶分布，使得在0~255灰阶上的分布更加均衡，提高了图像的对比度，
         * 达到改善图像主观视觉效果的目的。对比度较低的图像适合使用直方图均衡化方法来增强图像细节。
         */
        public void testHistogramEnhance () {
            setLibPath();
            String imageStr = imagePrePath + "8.jpg";
            Mat src = Imgcodecs.imread(imageStr);
            Mat dst = new Mat();

            List<Mat> matList = new LinkedList<>();
            matList.add(new Mat());
            matList.add(new Mat());
            matList.add(new Mat());
            HighGui.imshow("原图像", src);
            Core.split(src, matList);
            for (int i = 0; i < 3; i++) {
                Imgproc.equalizeHist(matList.get(i), matList.get(i));
            }
            Core.merge(matList, dst);

            HighGui.imshow("直方图均衡化图像增强效果", dst);
            HighGui.waitKey();
        }


        /**
         * 图像缩放 https://blog.csdn.net/m1109048058/article/details/77069607
         */
        public void testResize () {
            setLibPath();
            String imageStr = imagePrePath + "2.jpg";
            File imgFile = new File(imageStr);
            Mat src = Imgcodecs.imread(imageStr);
            Mat dst = new Mat();
            Imgproc.resize(src, dst, new Size(src.cols() / 2, src.rows() / 2), 0, 0, Imgproc.INTER_AREA);
            Imgcodecs.imwrite(imagePrePath + "narrow.jpg", dst);

            Mat endst = new Mat();
            Imgproc.resize(src, endst, new Size(src.cols() * 2, src.rows() * 2), 0, 0, Imgproc.INTER_LINEAR);
            Imgcodecs.imwrite(imagePrePath + "enlarge.jpg", endst);
        }
    }
